# Xmlbuilder单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|                        接口名                        | 是否通过	 | 备注  |
|:-------------------------------------------------:|:-----:|:---:|
|          create(type:string,opt:Option)           | pass  |     |
|     ele(name:string,ele:string,obj:ESObject)      | pass  |     |
|                end(opt?:ESObject)                 | pass  |     |
|          att(name:string,content:string)          | pass  |     |
|           removeAttribute(name:string)            | pass  |     |
|               cdata(content:string)               | pass  |     |
|                raw(content:string)                | pass  |     |
|                com(content:string)                | pass  |     |
|          ins(name:string,content:string)          | pass  |     |
|            commentBefore(name:string)             | pass  |     |
|             commentAfter(name:string）             | pass  |     |
|                  e(name:string)                   | pass  |     |
|                        u()                        | pass  |     |
|           i(name:string,content:string)           | pass  |     |
|           a(name:string,content:string)           | pass  |     |
|                  t(name:string)                   | pass  |     |
|           c(name:string,content:string)           | pass  |     |
|                  r(name:string)                   | pass  |     |
