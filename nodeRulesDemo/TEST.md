# nodeRulesDemo单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|                         接口名                         | 是否通过	 | 备注  |
|:---------------------------------------------------:|:-----:|:---:|
|                register(rules: Rule)                | pass  |     |
|                When(outcome:boolean)                | pass  |     |
|                       stop()                        | pass  |     |
|                       next()                        | pass  |     |
|                      restart()                      | pass  |     |
| execute(fact: Fact, callback: (fact: Fact) => void) | pass  |     |

