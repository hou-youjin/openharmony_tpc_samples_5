## v2.0.0

- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs 语法适配

## v1.0.2

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.0.1

1. api8升级到api9，并转换为stage模型
2. 修复设备端与PC端无法通信的问题

## v1.0.0

 - 实现功能

   1. 支持创建STUN server

   2. 支持创建STUN client

   3. 支持获取NAT分配的外部IP地址和端口号

   4. 支持获取NAT的行为类型
