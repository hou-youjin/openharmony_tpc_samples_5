## EventEmitter3单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/primus/eventemitter3/blob/master/test/test.js)
进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|eventNames|pass||
|listeners|pass||
|listenerCount|pass||
|emit|pass||
|on|pass||
|addListener|pass||
|once|pass||
|removeListener|pass||
|off|pass||
|removeAllListeners|pass||