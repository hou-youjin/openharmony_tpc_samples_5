/**
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */

import { describe, it, expect } from '@ohos/hypium'
import parse from 'minimist'

export default function unknownTest() {
  describe('minimist_unknown', ()=> {
    it('boolean_and_alias_is_not_unknown', 0, ()=> {
      let unknown: ESObject = [];
      let unknownFn = (arg: ESObject) => {
        unknown.push(arg);
        return false;
      }
      let aliased = ['-h', 'true', '--derp', 'true'];
      let regular = ['--herp', 'true', '-d', 'true'];
      let opts: ESObject = {
        alias: {
          h: 'herp'
        } as ESObject,
        boolean: 'h',
        string:"_",
        unknown: unknownFn
      };
      parse(aliased, opts);
      parse(regular, opts);

      expect(unknown).assertDeepEquals(['--derp', '-d']);
    });

    it('flag_boolean_true_any_double_hyphen_argument_is_not_unknown', 0, ()=> {
      let unknown : ESObject= [];
      let unknownFn = (arg: ESObject) => {
        unknown.push(arg);
        return false;
      }
      let argv :string[] = parse(['--honk', '--tacos=good', 'cow', '-p', '55'], {
        boolean: true,string:"_",
        unknown: unknownFn
      });
      expect(unknown).assertDeepEquals(['--tacos=good', 'cow', '-p']);
      expect(argv).assertDeepEquals({
        honk: true,
        _: []
      });
    });

  })
}