# text-encoding单元测试用例

单元测试用例覆盖情况

|                             接口名                             |是否通过	|备注|
|:-----------------------------------------------------------:|:---:|:---:|
|                        removeSubsets                        |pass   |        |
|                   compareDocumentPosition                   |pass   |        |
|                         uniqueSort                          |pass   |        |
|                         getElements                         |pass   |        |
|                       getElementById                        |pass   |        |
|                    getElementsByTagName                     |pass  |     |
|                    getElementsByTagType                     |   pass  |          |
|                        removeElement                        | pass |  |
|                       replaceElement                        | pass  |       |
|                         appendChild                         |  pass |          |
|                           append                            |  pass |          |
|                        prependChild                         | pass  |          |
|                           filter                            | pass  |          |
|                            find                             |  pass |          |
|                           findOne                           |  pass |          |
|                            next                             | pass  |          |
|                          existsOne                          | pass  |          |
|                           findAll                           | pass  |          |
|                         textContent                         |  pass |          |
|                          innerText                          | pass  |          |
|                         getChildren                         | pass  |          |
|                          getParent                          | pass  |          |
|                         getSiblings                         |  pass |          |
|                      getAttributeValue                      | pass  |          |
|                          hasAttrib                          | pass  |          |
|                           getName                           | pass  |          |
|                     nextElementSibling                      | pass  |          |
|                     prevElementSibling                      | pass  |          |
